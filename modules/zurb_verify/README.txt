Module: Zurb Verify

Description
===========
Integrates the Zurb Verify http://verifyapp.com/

Installation
============
Copy the module directory in to your Drupal:
/sites/all/modules directory as usual.

Configuration
=============
visit admin/config/services/zurb_verify
Enter your Zurb Verify ID.

Documentation
=============
http://verifyapp.com/support
