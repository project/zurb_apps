Module: Zurb Apps

Description
===========
Zurb Apps http://zurb.com/apps
This module doesn't do anything, you need to
enable each sub-module individually.

Installation
============
Copy the module directory in to your Drupal:
/sites/all/modules directory as usual.
Enable each Zurb App sub-module individually.
